# Installing Jenkins on Cloud Server

Install Jenkins on DigitalOcean 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create an Ubuntu server on DigitalOcean 

* Set up and run Jenkins as Docker container 

* Intialize Jenkins 

## Steps 

Step 1: Create a digitalOcean droplet 

[Digital Ocean Droplet](/images/01_creating_jenkin_server.png)

Step 2: Configure firewwall on digitalOcean to enable ssh access 

[Firewall](/images/02_configuring_firewall_for_jenkins.png)

Step 3: Ssh into server and Download Docker on the cloud server 

     apt install docker.io 

[Installing Docker](/images/03_installing_docker_on_server.png)

Step 4: Run Jenkins as a docker container 

     docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins-home jenkins/jenkins:lts

[Jenkins Container](/images/04_running_jenkins_as_a_container.png)

Step 5: Initialize Jenkins 

[Jenkins Browser](/images/05_jenkins_on_browser.png)
[Getting Password](/images/06_getting_jenkins_initial_password.png)

## Installation

apt install docker.io

## Usage 

docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins-home jenkins/jenkins:lts

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/installing-jenkins-on-digitalocean-cloud-server.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review


## Tests

No test were ran 

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/installing-jenkins-on-digitalocean-cloud-server

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.